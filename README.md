# Computer's architecture semestral work
#### project team: 
 Matvej Safrankov
 Daniil Partsiankou 

#### Theme: 
Dungeon-crawler 

#### Main task: 
Write Rogue inspired game on the Microzed control unit. 

#### Implemented features:
- Character movement using control knobs
- Menu using one control knob
- LED blinking when hitting something

#### Short introduction: 
Rogue is a game where you have to navigate through several rooms of a dungeon and your task is to fight monsters. When your last blood is spilled, the game ends.

The game will display your HP (health points)

---

# Control & HP:

Navigation is done by using the buttons. We have three buttons: red, green and blue. With the red button we move the player vertically, to the right is up and to the left is down respectively. With the blue button we move the player horizontally, turning the button to the right and left.

The health points are displayed on a LED panel. ![hp](uploads/a68eb7300ef415937b08a05c5def92e8/hp.jpg)

# Menu:

In the menu you can choose two levels. The levels differ in structure and the amount of monsters in them.
Example of the level:
![photo_2023-05-31_19-35-57](uploads/e8aa482d6ac8b9e8e2c87acbfd5c7b00/photo_2023-05-31_19-35-57.jpg)

The green button is used to control the menu. Press the red button to return to the menu from the game. 

# Game:

We play as a character that has 4 life points. On the map you will find enemies who remove one point (be careful, you can die) and healing potions that regenerate all of the missing health points(one health point is 8 LED lights).