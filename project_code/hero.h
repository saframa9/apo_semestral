// Structure representing the hero
typedef struct
{
	int x;              // Hero's x-coordinate on the map
	int y;              // Hero's y-coordinate on the map
	uint32_t HP;        // Hero's health points
	uint32_t minus_HP;  // Health points to be subtracted in a fight
} hero;