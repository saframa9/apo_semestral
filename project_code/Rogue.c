/*******************************************************************
  Project main function template for MicroZed based MZ_APO board
  designed by Petr Porazil at PiKRON

  Game implementation by:
  - Matvej Safrankov
  - Daniil Partsiankou
 *******************************************************************/
// STDANDART LIBRARIES ====================================================================================
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <math.h>

// MZAPO HARDWARE LIBRARIES ================================================================================
#include "mzapo_phys.h"
#include "mzapo_regs.h"

// OUR LIBRARIES ==========================================================================================
#include "movement.h"
#include "knobs.h"
#include "map.h"
#include "macros.h"

// MAIN ====================================================================================================
int main(int argc, char *argv[])
{
	printf("Hello\n");
	unsigned char *parperifery_mem_base;
	unsigned char *mem_base;
	unsigned char *lcd_mem_base;
	int game_status = MENU_STATUS;
	int option = 1;

	mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
	lcd_mem_base = map_init();
	while (1)
	{
		// MENU ====================================================================================================
		if (game_status == MENU_STATUS)
		{
			while (1)
			{
				lcd_mem_base = redraw_menu(menu, lcd_mem_base, option);

                //Accessing physical memory to read knobs
				parperifery_mem_base = mem_base;
				uint32_t saved_knobs = *(uint32_t *)(parperifery_mem_base + SPILED_REG_KNOBS_8BIT_o);

				char input = wait_for_knob_input(parperifery_mem_base, saved_knobs);
				if (input == GREEN_UP)
				{
					option--;
					if (option < 1)
					{
						option = 3;
					}
				}
				else if (input == GREEN_DOWN)
				{
					option++;
					if (option > 3)
					{
						option = 1;
					}
				}
				else if (input == GREEN_PRESS)
				{
					if (option == 1)
					{
						game_status = 1;
						break;
					}
					else if (option == 2)
					{
						game_status = 2;
						break;
					}
					else if (option == 3)
					{
						exit(0);
					}
				}
			}
		}
		// GAME ====================================================================================================
		else
		{
			if (game_status == MAP1_STATUS)
			{
				fetch_map(map1, map);
			}
			else
			{
				fetch_map(map2, map);
			}

			hero Hero = {4, 17, 4294967295, 4294967295};
			map[COORD(Hero.x, Hero.y)] = HERO_GLYPH_ID; // place hero

			while (1)
			{
                //Accessing physical memory to read knobs
				parperifery_mem_base = mem_base;
				uint32_t saved_knobs = *(volatile uint32_t *)(parperifery_mem_base + SPILED_REG_KNOBS_8BIT_o);

				char input = wait_for_knob_input(parperifery_mem_base, saved_knobs);
				Hero = move_hero(input, Hero, map);
				if (input == RED_PRESS)
				{
					game_status = 0;
					break;
				}
				map[COORD(Hero.x, Hero.y)] = HERO_GLYPH_ID;
				*(volatile uint32_t *)(mem_base + SPILED_REG_LED_LINE_o) = Hero.HP;
				if (Hero.HP == 0)
				{
					printf("You died\n Noob\n");
					game_status = MENU_STATUS;
					break;
				}
				lcd_mem_base = redraw_map(map, lcd_mem_base);
			}
		}
	}

	printf("Goodbye\n");
	exit(0);
}
