#include "knobs.h"

#ifndef CLOCK_MONOTONIC
#define CLOCK_MONOTONIC 1
#endif

char wait_for_knob_input(unsigned char *mem_base, uint32_t saved_knobs)
{
	uint8_t saved_knob_press_g = ((saved_knobs >> 25) & 0x1);
	uint8_t saved_knob_press_b = ((saved_knobs >> 24) & 0x1);
	uint8_t saved_knob_press_r = ((saved_knobs >> 26) & 0x1);
	uint8_t saved_red_knob = ((saved_knobs >> 16) & 0xFF);
	uint8_t saved_green_knob = ((saved_knobs >> 8) & 0xFF);
	uint8_t saved_blue_knob = ((saved_knobs)&0xFF);

	struct timespec loop_delay = {.tv_sec = 0, .tv_nsec = 120 * 1000 * 1000};
	while (1)
	{
		// mem_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE, 0);
		uint32_t current_all_knobs = *(uint32_t *)(mem_base + SPILED_REG_KNOBS_8BIT_o);

		if (saved_knobs != current_all_knobs)
		{
			char direction;

			uint8_t current_knob_press_g = ((current_all_knobs >> 25) & 0x1);
			uint8_t current_knob_press_b = ((current_all_knobs >> 24) & 0x1);
			uint8_t current_knob_press_r = ((current_all_knobs >> 26) & 0x1);
			uint8_t current_red_knob = ((current_all_knobs >> 16) & 0xFF);
			uint8_t current_green_knob = ((current_all_knobs >> 8) & 0xFF);
			uint8_t current_blue_knob = ((current_all_knobs)&0xFF);

			if (current_knob_press_g != saved_knob_press_g)
			{
				direction = GREEN_PRESS;
			}
			if (current_knob_press_b != saved_knob_press_b)
			{
				direction = BLUE_PRESS;
			}
			if (current_knob_press_r != saved_knob_press_r)
			{
				direction = RED_PRESS;
			}
			if (current_red_knob != saved_red_knob)
			{
				if (current_red_knob < saved_red_knob)
				{
					direction = RED_DOWN;
				}
				else
				{
					direction = RED_UP;
				}
			}
			if (current_green_knob != saved_green_knob)
			{
				if (current_green_knob < saved_green_knob)
				{
					direction = GREEN_DOWN;
				}
				else
				{
					direction = GREEN_UP;
				}
			}
			if (current_blue_knob != saved_blue_knob)
			{
				if (current_blue_knob < saved_blue_knob)
				{
					direction = BLUE_DOWN;
				}
				else
				{
					direction = BLUE_UP;
				}
			}

			return direction;
		}

		clock_nanosleep(CLOCK_MONOTONIC, 0, &loop_delay, NULL);
	}
}
