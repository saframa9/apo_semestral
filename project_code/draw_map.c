#include "draw_map.h"

unsigned short fb[320 * 480];
font_descriptor_t *fdes;

unsigned int hsv2rgb_lcd(int hue, int saturation, int value)
{
    hue = (hue % 360);
    float f = ((hue % 60) / 60.0);
    int p = (value * (255 - saturation)) / 255;
    int q = (value * (255 - (saturation * f))) / 255;
    int t = (value * (255 - (saturation * (1.0 - f)))) / 255;
    unsigned int r, g, b;
    if (hue < 60)
    {
        r = value;
        g = t;
        b = p;
    }
    else if (hue < 120)
    {
        r = q;
        g = value;
        b = p;
    }
    else if (hue < 180)
    {
        r = p;
        g = value;
        b = t;
    }
    else if (hue < 240)
    {
        r = p;
        g = q;
        b = value;
    }
    else if (hue < 300)
    {
        r = t;
        g = p;
        b = value;
    }
    else
    {
        r = value;
        g = p;
        b = q;
    }
    r >>= 3;
    g >>= 2;
    b >>= 3;
    return (((r & 0x1f) << 11) | ((g & 0x3f) << 5) | (b & 0x1f));
}

void draw_pixel(int x, int y, unsigned short color)
{
    if (x >= 0 && x < SCREEN_WIDTH && y >= 0 && y < SCREEN_HEIGHT)
    {
        fb[S_COORD(x,y)] = color;
    }
}

void draw_pixel_big(int x, int y, unsigned short color, int scale)
{
    for (int i = 0; i < scale; i++)
    {
        for (int j = 0; j < scale; j++)
        {
            draw_pixel(x + i, y + j, color);
        }
    }
}

int char_width(int ch)
{
    int width;
    if (!fdes->width)
    {
        width = fdes->maxwidth;
    }
    else
    {
        width = fdes->width[ch - fdes->firstchar];
    }
    return width;
}

void draw_char(int x, int y, char ch, unsigned short color, int scale)
{
    int w = char_width(ch);
    const font_bits_t *ptr;
    if ((ch >= fdes->firstchar) && (ch - fdes->firstchar < fdes->size))
    {
        if (fdes->offset)
        {
            ptr = &fdes->bits[fdes->offset[ch - fdes->firstchar]];
        }
        else
        {
            int bw = (fdes->maxwidth + 15) / 16;
            ptr = &fdes->bits[(ch - fdes->firstchar) * bw * fdes->height];
        }
        int i, j;
        for (i = 0; i < fdes->height; i++)
        {
            font_bits_t val = *ptr;
            for (j = 0; j < w; j++)
            {
                if ((val & 0x8000) != 0)
                {
                    draw_pixel_big(x + scale * j, y + scale * i, color, scale);
                }
                val <<= 1;
            }
            ptr++;
        }
    }
}

char choose_glyph(unsigned char glyph)
{
    switch (glyph)
    {
    case WALL_GLYPH_ID:
        return WALL_GLYPH;
        break;
    case ENEMY_GLYPH_ID:
        return ENEMY_GLYPH;
        break;
    case HEAL_GLYPH_ID:
        return HEAL_GLYPH;
        break;
    case HERO_GLYPH_ID:
        return HERO_GLYPH;
        break;
    default:
        return 0;
        break;
    }
}

unsigned char *map_init()
{
    unsigned char *parlcd_mem_base;
    parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
    if (parlcd_mem_base == NULL)
        exit(1);

    parlcd_hx8357_init(parlcd_mem_base);

    return parlcd_mem_base;
}

unsigned char *redraw_map(unsigned char map[], unsigned char *mem_base)
{
    int scale = 1;
    fdes = &font_rom8x16;

    for (int ptr = 0; ptr < SCREEN_SIZE; ptr++)
    {
        fb[ptr] = 0;
    }

    for (int y = 0; y < SCREEN_HEIGHT; y++)
    {
        for (int x = 0; x < SCREEN_WIDTH; x++)
        {
            if ((y % 16 == 0) && (x % 8 == 0))
            {
                draw_char(x , y, choose_glyph(map[RECALC_COORD(x, y)]), hsv2rgb_lcd(255, 0, 255), scale);
                parlcd_write_cmd(mem_base, 0x2c);
            }
        }
    }

    for (int ptr = 0; ptr < SCREEN_SIZE; ptr++)
    {
        parlcd_write_data(mem_base, fb[ptr]);
    }

    return mem_base;
}

unsigned char *redraw_menu(unsigned char menu[], unsigned char *mem_base, int option)
{
    int scale = 2;
    fdes = &font_rom8x16;

    for (int ptr = 0; ptr < SCREEN_SIZE; ptr++)
    {
        fb[ptr] = 0;
    }

    for (int y = 0; y < MENU_WIDTH; ++y)
    {
        for (int x = 0; x < MENU_HEIGHT; ++x)
        {
            unsigned int color = 0;
            if (option == 1 && y == 2)
            {
                color = hsv2rgb_lcd(110, 150, 93);
            }
            else if (option == 2 && y == 4)
            {
                color = hsv2rgb_lcd(110, 150, 93);
            }
            else if (option == 3 && y == 6)
            {
                color = hsv2rgb_lcd(110, 150, 93);
            }
            else
            {
                color = hsv2rgb_lcd(255, 0, 255);
            }

            if (menu[y * 30 + x] != 0)
            {
                draw_char(x * 8 * scale, y * 16 * scale, menu[y * 30 + x], color, scale);
            }
            parlcd_write_cmd(mem_base, 0x2c);
        }
    }

    for (int ptr = 0; ptr < SCREEN_SIZE; ptr++)
    {
        parlcd_write_data(mem_base, fb[ptr]);
    }

    return mem_base;
}

unsigned char *fetch_map(unsigned char map_to_fetch[], unsigned char map[])
{
    for (int m = 0; m < SCREEN_SIZE; ++m)
    {
        map[m] = map_to_fetch[m];
    }

    return map;
}
