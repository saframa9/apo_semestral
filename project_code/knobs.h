#include <stdint.h>
#include <time.h>
#include <unistd.h>

#include "mzapo_phys.h"
#include "mzapo_regs.h"

#include "macros.h"

// Read the knob input and return the corresponding direction of the knob action
char wait_for_knob_input(unsigned char *mem_base, uint32_t saved_knobs);