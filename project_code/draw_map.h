#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>

#include "mzapo_parlcd.h"
#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "font_types.h"
#include "hero.h"
#include "macros.h"

// Convert HSV color values to RGB color value for LCD
unsigned int hsv2rgb_lcd(int hue, int saturation, int value);

// Draw a single pixel on the LCD screen
void draw_pixel(int x, int y, unsigned short color);

// Draw a pixel with scaling on the LCD screen
void draw_pixel_big(int x, int y, unsigned short color, int scale);

// Get the width of a character in the specified font
int char_width(int ch);

// Draw a character on the LCD screen using a specified font
void draw_char(int x, int y, char ch, unsigned short color, int scale);

// Choose the appropriate glyph based on the given glyph ID
char choose_glyph(unsigned char glyph);

// Initialize the LCD screen and return the memory base
unsigned char *map_init();

// Redraw the game map on the LCD screen
unsigned char *redraw_map(unsigned char map[], unsigned char *mem_base);

// Redraw the menu on the LCD screen based on the selected option
unsigned char *redraw_menu(unsigned char menu[], unsigned char *mem_base, int option);

// Fetch the specified map data and store it in the provided map array
unsigned char *fetch_map(unsigned char map_to_fetch[], unsigned char map[]);



