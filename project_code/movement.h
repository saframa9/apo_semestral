#include <stdint.h>

#include "draw_map.h"

// Clear the spot on the map where the Hero is located
void clear_spot(hero Hero, unsigned char map[]);

// Decrease Hero's health points
hero get_hit(hero Hero);

// Restore Hero's health points to maximum
hero get_heal(hero Hero);

// Move the Hero on the map based on the provided direction
hero move_hero(char direction, hero Hero, unsigned char map[]);