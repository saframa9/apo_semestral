#define GREEN_PRESS 'o'
#define BLUE_PRESS 'p'
#define RED_PRESS 'i'

#define RED_UP '8'
#define RED_DOWN '2'
#define GREEN_UP '9'
#define GREEN_DOWN '3'
#define BLUE_UP '6'
#define BLUE_DOWN '4'

#define MENU_STATUS 0
#define MAP1_STATUS 1
#define MAP2_STATUS 2

#define ERROR_CALLOC_MSG "Error: Failed to allocate memory for map.\n"

#define SCREEN_WIDTH 480
#define SCREEN_HEIGHT 320
#define SCREEN_SIZE SCREEN_HEIGHT*SCREEN_WIDTH
#define S_COORD(x, y) ((y)*SCREEN_WIDTH + (x))

#define MAP_WIDTH 60
#define MAP_HEIGHT 20
#define COORD(x, y) ((y) * MAP_WIDTH + (x))
#define RECALC_COORD(x, y) (((x) / 8) + ((y) / 16) * MAP_WIDTH)

#define MENU_HEIGHT 40
#define MENU_WIDTH 120

#define WALL_GLYPH_ID 1
#define ENEMY_GLYPH_ID 2
#define HEAL_GLYPH_ID 3
#define HERO_GLYPH_ID 4

#define WALL_GLYPH 0x23
#define ENEMY_GLYPH 0x0c
#define HEAL_GLYPH 0x03
#define HERO_GLYPH 0x02