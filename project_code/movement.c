#include "movement.h"

void clear_spot(hero Hero, unsigned char map[])
{
	map[COORD(Hero.x, Hero.y)] = 0;
}

hero get_hit(hero Hero)
{
	Hero.minus_HP >>= 8;
	Hero.HP = Hero.minus_HP & Hero.HP;
	return Hero;
}

hero get_heal(hero Hero)
{
	Hero.HP = 4294967295;
	Hero.minus_HP = 4294967295;
	return Hero;
}

hero move_hero(char direction, hero Hero, unsigned char map[])
{

	clear_spot(Hero, map);
	switch (direction)
	{
	case (RED_UP):
	{
		if (map[COORD(Hero.x, Hero.y + 1)] != 1)
		{
			if (map[COORD(Hero.x, Hero.y + 1)] == 2)
			{
				Hero = get_hit(Hero);
			}
			else if (map[COORD(Hero.x, Hero.y + 1)] == 3)
			{
				Hero = get_heal(Hero);
			}
			Hero.y++;
		}
		break;
	}
	case (RED_DOWN):
	{
		if (map[COORD(Hero.x, Hero.y - 1)] != 1)
		{
			if (map[COORD(Hero.x, Hero.y - 1)] == 2)
			{
				Hero = get_hit(Hero);
			}
			else if (map[COORD(Hero.x, Hero.y -1)] == 3)
			{
				Hero = get_heal(Hero);
			}
			Hero.y--;
		}
		break;
	}
	case (BLUE_DOWN):
	{
		if (map[COORD(Hero.x - 1, Hero.y)] != 1)
		{
			if (map[COORD(Hero.x - 1, Hero.y)] == 2)
			{
				Hero = get_hit(Hero);
			}
			else if (map[COORD(Hero.x - 1, Hero.y)] == 3)
			{
				Hero = get_heal(Hero);
			}
			Hero.x--;
		}
		break;
	}
	case (BLUE_UP):
	{
		if (map[COORD(Hero.x + 1, Hero.y)] != 1)
		{
			if (map[COORD(Hero.x + 1, Hero.y)] == 2)
			{
				Hero = get_hit(Hero);
			}
			else if (map[COORD(Hero.x + 1, Hero.y)] == 3)
			{
				Hero = get_heal(Hero);
			}
			Hero.x++;
		}
		break;
	}
	}
	return Hero;
}
