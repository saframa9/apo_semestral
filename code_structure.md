```mermaid
classDiagram
class Rogue
Rogue : main game loop programe
Rogue : Menu()
Rogue : Game()

class Hero
Hero : int x
Hero : int y
Hero : uint32_t HP
Hero : uint32_t minus_HP

Rogue <-- movement
Rogue <-- knobs
movement <-- draw_map
draw_map <-- Hero

movement : moves hero around on the map
draw_map : draws everything on to screen menu / map

movement : void clear_spot(hero Hero, unsigned char map[])
movement : hero get_hit(hero Hero)
movement : hero get_heal(hero Hero)
movement : hero move_hero(char direction, hero Hero, unsigned char map[])

knobs : gets input from knobs
knobs : char wait_for_knob_input(unsigned char *mem_base, uint32_t saved_knobs)

draw_map : unsigned int hsv2rgb_lcd(int hue, int saturation, int value)
draw_map : void draw_pixel(int x, int y, unsigned short color)
draw_map : void draw_pixel_big(int x, int y, unsigned short color, int scale)
draw_map : int char_width(int ch)
draw_map : void draw_char(int x, int y, char ch, unsigned short color, int scale)
draw_map : char choose_glyph(unsigned char glyph)
draw_map : unsigned char *map_init()
draw_map : unsigned char *redraw_map(unsigned char map[], unsigned char *mem_base)
draw_map : unsigned char *redraw_menu(unsigned char menu[], unsigned char *mem_base, int option)
draw_map : unsigned char *fetch_map(unsigned char map_to_fetch[], unsigned char map[])
```
